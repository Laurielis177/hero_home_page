// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: constant_identifier_names

abstract class LocaleKeys {
  static const title = 'title';
  static const msg = 'msg';
  static const msg_named = 'msg_named';
  static const clickMe = 'clickMe';
  static const reset_password_label = 'reset_password.label';
  static const reset_password_username = 'reset_password.username';
  static const reset_password_password = 'reset_password.password';
  static const profile_reset_password = 'profile.reset_password';
  static const profile = 'profile';
  static const clicked = 'clicked';
  static const amount = 'amount';
  static const gender_with_arg = 'gender.with_arg';
  static const gender = 'gender';
  static const reset_locale = 'reset_locale';

  static const product_title_title = 'product_title.title';
  static const product_title_subtitle = 'product_title.subtitle';

  static const product_list_1 = 'product_list.1';
  static const product_list_2 = 'product_list.2';
  static const product_list_3 = 'product_list.3';

  static const feature_title_title = 'feature_title.title';
  static const feature_title_subtitle = 'feature_title.subtitle';

  static const product_card_title_1 = 'product_card.title_1';
  static const product_card_title_2 = 'product_card.title_2';
  static const product_card_title_3 = 'product_card.title_3';
  static const product_card_text_1 = 'product_card.text_1';
  static const product_card_text_2 = 'product_card.text_2';
  static const product_card_text_3 = 'product_card.text_3';
  static const video_title_title = 'video_title.title';
  static const company_title_title = 'company_title.title';
  static const crew_title_title = 'crew_title.title';
  static const company_intro_text_1 = 'company_intro.text_1';
  static const company_intro_text_2 = 'company_intro.text_2';
  static const contact_title_title = 'contact_title.title';
  static const contact_validate_text_name = 'contact_validate.text_name';
  static const contact_validate_text_email = 'contact_validate.text_email';
  static const contact_hint_text_name = 'contact_hint.text_name';
  static const contact_hint_text_email = 'contact_hint.text_email';
  static const contact_hint_text_message = 'contact_hint.text_message';
  static const contact_popup = 'contact_popup';
  static const contact_button = 'contact_button';
  static const footer_info_address = 'footer_info.address';
  static const company_award = 'company_award';
}

import 'package:flutter/material.dart';
import 'package:hero_home_page/components/ProductIntro/ProductIntro_Cards/index.dart';
import 'package:hero_home_page/components/ProductIntro/ProductIntro_List/index.dart';
import 'package:hero_home_page/components/Title/index.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:hero_home_page/generated/locale_keys.g.dart';
import 'package:hero_home_page/responsive/responsive_layout.dart';

class MyProductIntro extends StatefulWidget {
  const MyProductIntro({Key? key}) : super(key: key);

  @override
  State<MyProductIntro> createState() => _MyProductIntroState();
}

class _MyProductIntroState extends State<MyProductIntro> {
  @override
  Widget build(BuildContext context) {
    return ResponsiveLayout(
      mobileBody: Padding(
        padding: const EdgeInsets.symmetric(vertical: kDefaultPadding * 6),
        child: Column(
          children: const [
            MyTitle(
              title: LocaleKeys.product_title_title,
              subtitle: LocaleKeys.product_title_subtitle,
            ),
            MyProductIntroList(),
            Padding(
              padding: EdgeInsets.only(top: kDefaultPadding * 5),
              child: MyTitle(
                title: LocaleKeys.feature_title_title,
                subtitle: LocaleKeys.feature_title_subtitle,
              ),
            ),
            FractionallySizedBox(
                widthFactor: kDefaultWidthFactor, child: MyProductIntroCards()),
          ],
        ),
      ),
      tabletBody: Padding(
        padding: const EdgeInsets.symmetric(vertical: kDefaultPadding * 6),
        child: FractionallySizedBox(
          widthFactor: kDefaultWidthFactor,
          child: Column(
            children: const [
              MyTitle(
                title: LocaleKeys.product_title_title,
                subtitle: LocaleKeys.product_title_subtitle,
              ),
              MyProductIntroList(),
              Padding(
                padding: EdgeInsets.only(top: kDefaultPadding * 5),
                child: MyTitle(
                  title: LocaleKeys.feature_title_title,
                  subtitle: LocaleKeys.feature_title_subtitle,
                ),
              ),
              MyProductIntroCards(),
            ],
          ),
        ),
      ),
      desktopBody: Padding(
        padding: const EdgeInsets.symmetric(vertical: kDefaultPadding * 6),
        child: FractionallySizedBox(
          widthFactor: kDefaultWidthFactor,
          child: Column(
            children: const [
              MyTitle(
                title: LocaleKeys.product_title_title,
                subtitle: LocaleKeys.product_title_subtitle,
              ),
              MyProductIntroList(),
              Padding(
                padding: EdgeInsets.only(top: kDefaultPadding * 5),
                child: MyTitle(
                  title: LocaleKeys.feature_title_title,
                  subtitle: LocaleKeys.feature_title_subtitle,
                ),
              ),
              MyProductIntroCards(),
            ],
          ),
        ),
      ),
    );
  }
}

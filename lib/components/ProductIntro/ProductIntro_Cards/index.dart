import 'package:flutter/material.dart';
import 'package:hero_home_page/components/ProductIntro/ProductIntro_Cards/ProductIntro_Cards_Element/index.dart';
import 'package:hero_home_page/generated/locale_keys.g.dart';
import 'package:hero_home_page/responsive/responsive_layout.dart';
import 'package:responsive_grid/responsive_grid.dart';

class MyProductIntroCards extends StatelessWidget {
  const MyProductIntroCards({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {

    final List<MyData> myData = [
      MyData(
        title: LocaleKeys.product_card_title_1,
        text: LocaleKeys.product_card_text_1,
      ),
      MyData(
        title: LocaleKeys.product_card_title_2,
        text: LocaleKeys.product_card_text_2,
      ),
      MyData(
        title: LocaleKeys.product_card_title_3,
        text: LocaleKeys.product_card_text_3,
      ),
    ];
    return ResponsiveLayout(mobileBody:  Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        ResponsiveGridRow(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ResponsiveGridCol(
                lg: 4,
                xs: 12,
                child: MyProductIntroCardsElement(
                  index: 0,
                  myData: myData[0],
                )),
            ResponsiveGridCol(
              lg: 4,
              xs: 12,
              child: MyProductIntroCardsElement(
                index: 1,
                myData: myData[1],
              ),
            ),
            ResponsiveGridCol(
              lg: 4,
              xs: 12,
              child: MyProductIntroCardsElement(
                index: 2,
                myData: myData[2],
              ),
            ),
          ],
        ),
      ],
    ), tabletBody:  Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        ResponsiveGridRow(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ResponsiveGridCol(
                lg: 4,
                xs: 12,
                child: MyProductIntroCardsElement(
                  myData: myData[0], index: 0,
                )),
            ResponsiveGridCol(
              lg: 4,
              xs: 12,
              child: MyProductIntroCardsElement(
                myData: myData[1], index: 1,
              ),
            ),
            ResponsiveGridCol(
              lg: 4,
              xs: 12,
              child: MyProductIntroCardsElement(
                myData: myData[2], index: 2,
              ),
            ),
          ],
        ),
      ],
    ), desktopBody:  Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        ResponsiveGridRow(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ResponsiveGridCol(
                lg: 4,
                xs: 12,
                child: MyProductIntroCardsElement(
                  myData: myData[0], index: 0,
                )),
            ResponsiveGridCol(
              lg: 4,
              xs: 12,
              child: MyProductIntroCardsElement(
                myData: myData[1], index: 1,
              ),
            ),
            ResponsiveGridCol(
              lg: 4,
              xs: 12,
              child: MyProductIntroCardsElement(
                myData: myData[2], index: 2,
              ),
            ),
          ],
        ),
      ],
    ),);
  }
}

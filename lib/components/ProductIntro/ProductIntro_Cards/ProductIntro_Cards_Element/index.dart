import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hero_home_page/colorScheme/constants.dart';

class MyProductIntroCardsElement extends StatelessWidget {
  const MyProductIntroCardsElement({super.key,required this.myData, required this.index});
  final MyData myData;
  final int index;
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    const List<IconData> myIcon = [Icons.location_on, Icons.public , Icons.offline_bolt];

    return Padding(
      padding:EdgeInsets.symmetric(horizontal: width / 300, vertical: height / 250),
      child: Center(
        child: Card(
          elevation: 5,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: kDefaultPadding),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: kDefaultPadding / 2),
                  child: Icon(
                    myIcon[index],
                    size: 100.0,
                    color: kPrimary,
                  ),
                ),
                ListTile(
                  title: Text(
                    myData.title,
                    textAlign: TextAlign.center,
                    maxLines: 1,
                    style: Theme
                        .of(context)
                        .textTheme
                        .headlineSmall,
                    ).tr(),
                  ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: kDefaultPadding * 2),
                  child: Divider(
                    color: kGrey,
                    thickness: 3,
                  ),
                ),
                ListTile(
                  subtitle: Text(
                    myData.text,
                    style:  Theme
                        .of(context)
                        .textTheme
                        .titleMedium,
                    textAlign: TextAlign.center,
                    maxLines: 2,
                  ).tr(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class MyData {
  String title;
  String text;
  MyData({
    required this.title,
    required this.text,
  });
}


import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hero_home_page/generated/locale_keys.g.dart';
import 'package:hero_home_page/responsive/responsive_layout.dart';
import 'package:easy_localization/easy_localization.dart';

class MyProductIntroList extends StatelessWidget {
  const MyProductIntroList({Key? key}) : super(key: key);

  final String assetName = 'assets/Icons/logo.svg';
  @override
  Widget build(BuildContext context) {
    return ResponsiveLayout(
      mobileBody: Column(
        children: <Widget>[
          SvgPicture.asset(
            assetName,
            height: 200.0,
            width: 200.0,
          ),
          AlertDialog(
            elevation: 5,
            content: Column(
              children: [
                ListTile(
                  title: Text(
                    LocaleKeys.product_list_1.tr(),
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  leading: const Icon(Icons.filter_1_outlined),
                ),
                ListTile(
                  title: Text(
                    LocaleKeys.product_list_2.tr(),
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  leading: const Icon(Icons.filter_2_outlined),
                ),
                ListTile(
                  title: Text(
                    LocaleKeys.product_list_3.tr(),
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  leading: const Icon(Icons.filter_3_outlined),
                ),
              ],
            ),
          ),
        ],
      ),
      tabletBody: Row(
        children: <Widget>[
          SvgPicture.asset(
            assetName,
            height: 200.0,
            width: 200.0,
          ),
          Expanded(
            child: AlertDialog(
              elevation: 5,
              content: Column(
                children: [
                  ListTile(
                    title: Text(
                      LocaleKeys.product_list_1.tr(),
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    leading: const Icon(Icons.filter_1_outlined),
                  ),
                  ListTile(
                    title: Text(
                      LocaleKeys.product_list_2.tr(),
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    leading: const Icon(Icons.filter_2_outlined),
                  ),
                  ListTile(
                    title: Text(
                      LocaleKeys.product_list_3.tr(),
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    leading: const Icon(Icons.filter_3_outlined),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      desktopBody: Row(
        children: <Widget>[
          SvgPicture.asset(
            assetName,
            height: 200.0,
            width: 200.0,
          ),
          Expanded(
            child: AlertDialog(
              elevation: 5,
              content: Column(
                children: [
                  ListTile(
                    title: Text(
                      LocaleKeys.product_list_1.tr(),
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    leading: const Icon(Icons.filter_1_outlined),
                  ),
                  ListTile(
                    title: Text(
                      LocaleKeys.product_list_2.tr(),
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    leading: const Icon(Icons.filter_2_outlined),
                  ),
                  ListTile(
                    title: Text(
                      LocaleKeys.product_list_3.tr(),
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    leading: const Icon(Icons.filter_3_outlined),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

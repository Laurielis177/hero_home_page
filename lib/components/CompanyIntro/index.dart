import 'package:flutter/material.dart';
import 'package:hero_home_page/components/Title/index.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:hero_home_page/generated/locale_keys.g.dart';

class MyCompanyIntro extends StatelessWidget {
  const MyCompanyIntro({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textScaleFactor = MediaQuery.of(context).textScaleFactor;
    return Center(
      child: FractionallySizedBox(
        widthFactor: kDefaultWidthFactor,
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: kDefaultPadding * textScaleFactor * 6,
          ),
          child: Column(
            children: [
              MyTitle(title: LocaleKeys.company_title_title.tr(), subtitle: ''),
              Column(
                children: [
                  Text(
                    LocaleKeys.company_intro_text_1.tr(),
                    style: Theme.of(context).textTheme.titleLarge,
                  ).tr(),
                  Text(
                    LocaleKeys.company_intro_text_2.tr(),
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

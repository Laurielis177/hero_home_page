import 'dart:async';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:hero_home_page/responsive/responsive_layout.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

class MyHeroPage extends StatefulWidget {
  const MyHeroPage({Key? key}) : super(key: key);

  @override
  State<MyHeroPage> createState() => _MyHeroPageState();
}

class _MyHeroPageState extends State<MyHeroPage> {
  late int _current = 0;
  final CarouselController _controller = CarouselController();
  //============================================================================
  List<ParseObject> heroPageList = [];
  List<ParseObject> headingList = [];

  StreamController<List<ParseObject>> streamController = StreamController();
//==============================================================================

  final QueryBuilder<ParseObject> queryHeroPage =
      QueryBuilder<ParseObject>(ParseObject('HeroPage'))
        ..orderByAscending('index');

  final LiveQuery liveQuery = LiveQuery(debug: true);
  late Subscription<ParseObject> subscription;

  bool isLoading = true;

  void startTimer() {
    Timer.periodic(const Duration(seconds: 2), (t) {
      setState(() {
        isLoading = false;
      });
      t.cancel();
    });
  }

  void startLiveQuery() async {
    subscription = await liveQuery.client.subscribe(queryHeroPage);

    subscription.on(LiveQueryEvent.create, (value) {
      debugPrint('*** CREATE ***: $value ');
      heroPageList.add(value);
      streamController.add(heroPageList);
    });

    subscription.on(LiveQueryEvent.update, (value) {
      debugPrint('*** UPDATE ***: $value ');
      heroPageList[heroPageList
          .indexWhere((element) => element.objectId == value.objectId)] = value;
      streamController.add(heroPageList);
    });

    subscription.on(LiveQueryEvent.delete, (value) {
      debugPrint('*** DELETE ***: $value ');
      heroPageList.removeWhere((element) => element.objectId == value.objectId);
      streamController.add(heroPageList);
    });
  }

  void cancelLiveQuery() async {
    liveQuery.client.unSubscribe(subscription);
  }

  void getHeroPageList() async {
    final ParseResponse apiResponse = await queryHeroPage.query();

    if (apiResponse.success && apiResponse.results != null) {
      streamController.add(apiResponse.results as List<ParseObject>);
    } else {
      streamController.add([]);
    }
  }

  // first time query in Award
  void _getParseData() {
    queryHeroPage.query().then((apiResponse) => {
          streamController.add(apiResponse.results as List<ParseObject>),
          if (apiResponse.success && apiResponse.results != null)
            {
              for (var o in apiResponse.results!)
                {
                  _headingPointer((o as ParseObject).get("heading")),
                  setState(() {
                    heroPageList.add(o);
                  }),
                },
            }
        });
  }

  void _headingPointer(languageParse) {
    ParseObject lang = languageParse;

    final QueryBuilder<ParseObject> parseQuery =
        QueryBuilder<ParseObject>(ParseObject('Language'));

    parseQuery.whereContains('objectId', lang.objectId!);
    parseQuery.query().then((response) => {
          if (response.success && response.results != null)
            {
              for (var o in response.results!)
                {
                  setState(() {
                    headingList.add(o);
                  }),
                }
            }
        });
  }



  @override
  void initState() {
    super.initState();
    _getParseData();
    getHeroPageList();
    startLiveQuery();
    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    final textScaleFactor = MediaQuery.of(context).textScaleFactor;
    // get the Locale change & setState
    final String switchLanguage = context.locale.toString();
    setState(() {
      switchLanguage;
    });
    return ResponsiveLayout(
      mobileBody: SizedBox(
          child: isLoading
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Center(
                      child: SizedBox(
                          width: 100,
                          height: 100,
                          child: CircularProgressIndicator()),
                    ), //show this if state is loading
                  ],
                )
              : Stack(
                  children: [
                    CarouselSlider.builder(
                        carouselController: _controller,
                        options: CarouselOptions(
                            enlargeCenterPage: true,
                            height: MediaQuery.of(context).size.height,
                            viewportFraction: 1.0,
                            autoPlay: true,
                            onPageChanged: (index, reason) {
                              setState(() {
                                _current = index;
                              });
                            }),
                        itemCount: heroPageList.length,
                        itemBuilder:
                            (BuildContext context, int index, int realIndex) {
                          final double width =
                              MediaQuery.of(context).size.width;
                          final double height =
                              MediaQuery.of(context).size.height;
                          //*************************************
                          //Get Parse Object Values
                          final varHeroPage = heroPageList[index];
                          final varImage =
                              varHeroPage.get<ParseFileBase>('image');
                          final varHeading =
                              headingList[index].get<String>(switchLanguage);
                          //*************************************
                          return CarouselSlider(
                            options: CarouselOptions(
                              enlargeCenterPage: true,
                              height: height,
                              viewportFraction: 1.0,
                              autoPlay: true,
                            ),
                            items: heroPageList
                                .map(
                                  (item) => Center(
                                    child: Stack(
                                      children: [
                                        Image.network(
                                          varImage!.url!,
                                          fit: BoxFit.cover,
                                          color: Colors.black54,
                                          colorBlendMode: BlendMode.darken,
                                          width: width,
                                          height: height,
                                        ),
                                        Positioned(
                                            left: textScaleFactor * 10,
                                            right: textScaleFactor * 10,
                                            bottom: textScaleFactor * 300,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal:
                                                          kDefaultPadding * 2),
                                              child: SizedBox(
                                                width:
                                                    textScaleFactor * width / 5,
                                                child: AutoSizeText(
                                                  varHeading!,
                                                  maxLines: 3,
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    fontSize: kFontSubTitle *
                                                        textScaleFactor,
                                                    fontWeight: FontWeight.bold,
                                                    color: kOnPrimary,
                                                  ),
                                                ),
                                              ),
                                            ))
                                      ],
                                    ),
                                  ),
                                )
                                .toList(),
                          );
                        }),
                    Positioned(
                      right: textScaleFactor * 30,
                      left: textScaleFactor * 30,
                      bottom: kDefaultPadding,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: heroPageList.asMap().entries.map((entry) {
                          return GestureDetector(
                            onTap: () => _controller.animateToPage(entry.key),
                            child: Container(
                              width: 30,
                              height: 10.0,
                              margin: const EdgeInsets.symmetric(
                                  vertical: 2.0, horizontal: 4.0),
                              decoration: BoxDecoration(
                                  shape: (_current == entry.key
                                      ? BoxShape.circle
                                      : BoxShape.circle),
                                  color: (_current == entry.key
                                          ? kOnPrimary
                                          : kOnPrimary)
                                      .withOpacity(
                                          _current == entry.key ? 0.4 : 0.4)),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                )),
      tabletBody: SizedBox(
          child: isLoading
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Center(
                      child: SizedBox(
                          width: 100,
                          height: 100,
                          child: CircularProgressIndicator()),
                    ), //show this if state is loading
                  ],
                )
              : Stack(
                  children: [
                    CarouselSlider.builder(
                        carouselController: _controller,
                        options: CarouselOptions(
                            enlargeCenterPage: true,
                            height: MediaQuery.of(context).size.height,
                            viewportFraction: 1.0,
                            autoPlay: true,
                            onPageChanged: (index, reason) {
                              setState(() {
                                _current = index;
                              });
                            }),
                        itemCount: heroPageList.length,
                        itemBuilder:
                            (BuildContext context, int index, int realIndex) {
                          final double width =
                              MediaQuery.of(context).size.width;
                          final double height =
                              MediaQuery.of(context).size.height;
                          //*************************************
                          //Get Parse Object Values
                          final varHeroPage = heroPageList[index];
                          final varImage =
                              varHeroPage.get<ParseFileBase>('image');
                          final varHeading =
                              headingList[index].get<String>(switchLanguage);
                          //*************************************
                          return CarouselSlider(
                            options: CarouselOptions(
                              enlargeCenterPage: true,
                              height: height,
                              viewportFraction: 1.0,
                              autoPlay: true,
                            ),
                            items: heroPageList
                                .map(
                                  (item) => Center(
                                    child: Stack(
                                      children: [
                                        Image.network(
                                          varImage!.url!,
                                          fit: BoxFit.cover,
                                          color: Colors.black54,
                                          colorBlendMode: BlendMode.darken,
                                          width: width,
                                          height: height,
                                        ),
                                        Positioned(
                                          left: kDefaultPadding * 7,
                                          bottom: kDefaultPadding * 4,
                                          child: SizedBox(
                                            width:
                                                textScaleFactor * width / 1.7,
                                            child: AutoSizeText(
                                              varHeading!,
                                              maxLines: 2,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headlineMedium,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                                .toList(),
                          );
                        }),
                    Positioned(
                      right: textScaleFactor * 30,
                      left: textScaleFactor * 30,
                      bottom: kDefaultPadding,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: heroPageList.asMap().entries.map((entry) {
                          return GestureDetector(
                            onTap: () => _controller.animateToPage(entry.key),
                            child: Container(
                              width: 30,
                              height: 10.0,
                              margin: const EdgeInsets.symmetric(
                                  vertical: 2.0, horizontal: 4.0),
                              decoration: BoxDecoration(
                                  shape: (_current == entry.key
                                      ? BoxShape.circle
                                      : BoxShape.circle),
                                  color: (_current == entry.key
                                          ? kOnPrimary
                                          : kOnPrimary)
                                      .withOpacity(
                                          _current == entry.key ? 0.4 : 0.4)),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                )),
      desktopBody: SizedBox(
          child: isLoading
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Center(
                      child: SizedBox(
                          width: 100,
                          height: 100,
                          child: CircularProgressIndicator()),
                    ), //show this if state is loading
                  ],
                )
              : Stack(
                  children: [
                    CarouselSlider.builder(
                        carouselController: _controller,
                        options: CarouselOptions(
                            enlargeCenterPage: true,
                            height: MediaQuery.of(context).size.height,
                            viewportFraction: 1.0,
                            autoPlay: true,
                            onPageChanged: (index, reason) {
                              setState(() {
                                _current = index;
                              });
                            }),
                        itemCount: heroPageList.length,
                        itemBuilder:
                            (BuildContext context, int index, int realIndex) {
                          //*************************************
                          //Get Parse Object Values
                          final varHeroPage = heroPageList[index];
                          final varImage =
                              varHeroPage.get<ParseFileBase>('image');
                          final varHeading =
                              headingList[index].get<String>(switchLanguage);
                          //*************************************
                          final double width =
                              MediaQuery.of(context).size.width;
                          final double height =
                              MediaQuery.of(context).size.height;
                          return CarouselSlider(
                            options: CarouselOptions(
                              enlargeCenterPage: true,
                              height: height,
                              viewportFraction: 1.0,
                              autoPlay: true,
                            ),
                            items: heroPageList
                                .map(
                                  (item) => Center(
                                    child: Stack(
                                      children: [
                                        Image.network(
                                          varImage!.url!,
                                          fit: BoxFit.cover,
                                          color: Colors.black54,
                                          colorBlendMode: BlendMode.darken,
                                          width: width,
                                          height: height,
                                        ),
                                        Positioned(
                                          left: kDefaultPadding * 7,
                                          bottom: kDefaultPadding * 4,
                                          child: SizedBox(
                                            width:
                                                textScaleFactor * width / 1.7,
                                            child: AutoSizeText(
                                              varHeading!,
                                              maxLines: 2,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headlineMedium,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                                .toList(),
                          );
                        }),
                    Positioned(
                      bottom: 200,
                      left: 120,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: heroPageList.asMap().entries.map((entry) {
                          return GestureDetector(
                            onTap: () => _controller.animateToPage(entry.key),
                            child: Container(
                              width: 80,
                              height: 12.0,
                              margin: const EdgeInsets.symmetric(
                                  vertical: 8.0, horizontal: 4.0),
                              decoration: BoxDecoration(
                                  shape: (_current == entry.key
                                      ? BoxShape.rectangle
                                      : BoxShape.rectangle),
                                  color: (_current == entry.key
                                          ? kOnPrimary
                                          : kOnPrimary)
                                      .withOpacity(
                                          _current == entry.key ? 0.4 : 0.4)),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                )),
    );
  }

  @override
  void dispose() {
    cancelLiveQuery();
    streamController.close();
    super.dispose();
  }
}

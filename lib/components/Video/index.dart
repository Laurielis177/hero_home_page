import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:hero_home_page/components/Title/index.dart';
import 'package:hero_home_page/generated/locale_keys.g.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class MyVideo extends StatefulWidget {
  const MyVideo({super.key});

  @override
  State<MyVideo> createState() => _MyVideoState();
}

class _MyVideoState extends State<MyVideo> {
  late YoutubePlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = YoutubePlayerController(
      params: const YoutubePlayerParams(
        showControls: true,
        mute: false,
        showFullscreenButton: true,
        loop: false,
      ),
    )
      ..onInit = () {
        _controller.loadPlaylist(
          list: [
            // 'tcodrIK2P_I',
            'GU2Fx_ObtUc',
          ],
          listType: ListType.playlist,
          startSeconds: 12,
        );
      }
      ..onFullscreenChange = (isFullScreen) {
        log('${isFullScreen ? 'Entered' : 'Exited'} Fullscreen.');
      };
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return YoutubePlayerScaffold(
      controller: _controller,
      builder: (context, player) {
        return LayoutBuilder(
          builder: (context, constraints) {
            return Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: height * 0.15),
                  child: const MyTitle(
                      title: LocaleKeys.video_title_title, subtitle: ''),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    bottom: height * 0.15,
                    left: width * 0.1,
                    right: width * 0.1,
                  ),
                  child: player,
                ),
              ],
            );
          },
        );
      },
    );
  }

  @override
  void dispose() {
    _controller.close();
    super.dispose();
  }
}

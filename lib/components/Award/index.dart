import 'dart:core';
import 'package:flutter/material.dart';
import 'package:hero_home_page/components/Award/Award_Card/index.dart';
import 'package:hero_home_page/components/Title/index.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:hero_home_page/generated/locale_keys.g.dart';
import 'package:hero_home_page/responsive/responsive_layout.dart';

class MyAward extends StatefulWidget {
  const MyAward({Key? key}) : super(key: key);

  @override
  State<MyAward> createState() => _MyAwardState();
}

class _MyAwardState extends State<MyAward> {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        bottom: kDefaultPadding * 6,
        top: kDefaultPadding * 6,
      ),
      child: Column(
        children: const [
          MyTitle(title: LocaleKeys.company_award, subtitle: ''),
          ResponsiveLayout(
            desktopBody: FractionallySizedBox(
              widthFactor: kDefaultWidthFactor,
              child: MyAwardCard(),
            ),
            mobileBody: MyAwardCard(),
            tabletBody: FractionallySizedBox(
              widthFactor: kDefaultWidthFactor,
              child: MyAwardCard(),
            ),
          ),
        ],
      ),
    );
  }


}



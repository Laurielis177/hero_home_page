import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';
import 'package:timelines/timelines.dart';

class MyAwardCard extends StatefulWidget {
  const MyAwardCard({Key? key}) : super(key: key);

  @override
  State<MyAwardCard> createState() => _MyAwardCardState();
}

class _MyAwardCardState extends State<MyAwardCard> {
  late List<ParseObject> awardList = []; // get date, image
  late List<ParseObject> descriptionList = []; // get description
  late List<ParseObject> contentList = []; // get content

  StreamController<List<ParseObject>> streamController = StreamController();

  //============================================================================
  final LiveQuery liveQuery = LiveQuery(debug: true);
  late Subscription<ParseObject> subscription;

  final QueryBuilder<ParseObject> queryAward =
      QueryBuilder<ParseObject>(ParseObject('Award'))
        ..orderByDescending('date');

  @override
  void initState() {
    super.initState();
    getAwardList();
    startLiveQuery();
    _getParseData();
    startTimer();
  }

  void startLiveQuery() async {
    subscription = await liveQuery.client.subscribe(queryAward);

    subscription.on(LiveQueryEvent.create, (value) {
      debugPrint('*** CREATE ***: $value ');
      awardList.add(value);
      streamController.add(awardList);
    });

    subscription.on(LiveQueryEvent.update, (value) {
      debugPrint('*** UPDATE ***: $value ');
      awardList[awardList
          .indexWhere((element) => element.objectId == value.objectId)] = value;
      streamController.add(awardList);
    });

    subscription.on(LiveQueryEvent.delete, (value) {
      debugPrint('*** DELETE ***: $value ');
      awardList.removeWhere((element) => element.objectId == value.objectId);
      streamController.add(awardList);
    });
  }

  void cancelLiveQuery() async {
    liveQuery.client.unSubscribe(subscription);
  }

  void getAwardList() async {
    final ParseResponse apiResponse = await queryAward.query();

    if (apiResponse.success && apiResponse.results != null) {
      streamController.add(apiResponse.results as List<ParseObject>);
    } else {
      streamController.add([]);
    }
  }

  //============================================================================

  // first time query in Award
  void _getParseData() {
    final QueryBuilder<ParseObject> queryAward =
        QueryBuilder<ParseObject>(ParseObject('Award'));

    queryAward.query().then((apiResponse) => {
          if (apiResponse.success && apiResponse.results != null)
            {
              for (var o in apiResponse.results!)
                {
                  _descriptionPointer((o as ParseObject).get("description")),
                  _contentPointer((o).get("content")),
                  setState(() {
                    awardList.add(o);
                  }),
                },
            }
        });
  }

  // second time query in Lang to get the 'description' value
  void _descriptionPointer(languageParse) {
    ParseObject lang = languageParse;

    final QueryBuilder<ParseObject> queryAward =
        QueryBuilder<ParseObject>(ParseObject('Language'));

    queryAward.whereContains('objectId', lang.objectId!);
    queryAward.query().then((response) => {
          if (response.success && response.results != null)
            {
              for (var o in response.results!)
                {
                  setState(() {
                    descriptionList.add(o);
                  }),
                }
            }
        });
  }

  // second time query in Lang to get the 'content' value
  void _contentPointer(languageParse) {
    ParseObject lang = languageParse;

    final QueryBuilder<ParseObject> queryAward =
        QueryBuilder<ParseObject>(ParseObject('Language'));

    queryAward.whereContains('objectId', lang.objectId!);
    queryAward.query().then((response) => {
          if (response.success && response.results != null)
            {
              for (var o in response.results!)
                {
                  setState(() {
                    contentList.add(o);
                  }),
                }
            }
        });
  }

  bool isLoading = true;
  void startTimer() {
    Timer.periodic(const Duration(seconds: 2), (t) {
      setState(() {
        isLoading = false;
      });
      t.cancel();
    });
  }

  @override
  Widget build(BuildContext context) {
    // get the Locale change & setState
    final String switchLanguage = context.locale.toString();
    setState(() {
      switchLanguage;
    });
    return SizedBox(
        height: 1000,
        child: isLoading
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: const <Widget>[
                  Center(
                    child: SizedBox(
                        width: 100,
                        height: 100,
                        child: CircularProgressIndicator()),
                  ), //show this if state is loading
                ],
              )
            : Timeline.tileBuilder(
                primary: true,
                theme: TimelineThemeData(
                  direction: Axis.vertical,
                  color: kPrimary,
                ),
                builder: TimelineTileBuilder.fromStyle(
                  connectorStyle: ConnectorStyle.solidLine,
                  indicatorStyle: IndicatorStyle.dot,
                  contentsAlign: ContentsAlign.alternating,
                  contentsBuilder: (context, index) {
//*************************************
                    final varAward = awardList[index];
                    final varDate = varAward.get<DateTime>('date');
                    final varImage = varAward.get<ParseFileBase>('image');
                    final varDesc =
                        descriptionList[index].get<String>(switchLanguage);
                    final varContent =
                        contentList[index].get<String>(switchLanguage);
//*************************************
                    return Card(
                      clipBehavior: Clip.antiAlias,
                      child: Column(
                        children: [
                          ListTile(
                            title: Text(
                              DateFormat.yM().format(varDate!),
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Image.network(
                            varImage!.url!,
                            fit: BoxFit.cover,
                          ),
                          Expanded(
                            child: ListTile(
                              subtitle: Text(
                                varDesc!,
                                style: const TextStyle(color: kOnBackground),
                              ),
                              onTap: () => showDialog<String>(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                  title: Text(varDesc),
                                  content: SingleChildScrollView(
                                    child: Column(
                                      children: [
                                        Image.network(
                                          varImage.url!,
                                          fit: BoxFit.cover,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: kDefaultPadding),
                                          child: Text(varContent!),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                  itemCount: awardList.length,
                )));
  }

  @override
  void dispose() {
    super.dispose();
  }
}

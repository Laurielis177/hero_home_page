import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hero_home_page/colorScheme/constants.dart';

class MyHeader extends StatelessWidget {
  final String title;

  const MyHeader({Key? key, required this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height / 7 * 2;
    final textScaleFactor = MediaQuery.textScaleFactorOf(context);
    return SizedBox(
      height: height,
        child: Stack(children: [
          Image(
            image: const AssetImage(
              "assets/images/carousel3.jpg",
            ),
            color: kOnBackground,
            colorBlendMode: BlendMode.multiply,
            fit: BoxFit.cover,
            width: width,
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: kDefaultPadding * 6, left: kDefaultPadding * 4),
            child: Text(
              title,
              style: TextStyle(
                  fontSize: kFontTitle * textScaleFactor / 1.2,
                  color: kBackground,
                  fontWeight: FontWeight.bold),
            ).tr(),
          ),
        ]));
  }
}

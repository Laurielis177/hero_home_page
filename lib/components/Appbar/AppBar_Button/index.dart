import 'package:flutter/material.dart';
import 'package:hero_home_page/colorScheme/constants.dart';

class MyAppBarButton extends StatefulWidget {
  const MyAppBarButton(
      {Key? key, required this.index, required this.scrollOffset})
      : super(key: key);
  final int index;
  final double scrollOffset;

  @override
  State<MyAppBarButton> createState() => _MyAppBarButtonState();
}

class _MyAppBarButtonState extends State<MyAppBarButton> {
  final List<String> title = ['HOME', 'ABOUT', 'PRODUCT', 'CONTACT'];
  final List<bool> hovering = [false, false, false, false];
  final List<String> pages = ['/Home', '/About', '/Product', '/Contact'];
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return Container(
      height: height,
      decoration: _boxDecoration(),
      child: TextButton(
        onPressed: () {
          setState(() {
            Navigator.pushNamed(context, pages[widget.index]);
          });
        },
        onHover: (hovered) {
          setState(() {
            hovering[widget.index] = hovered;
          });
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
          child: Text(title[widget.index], style: _textStyle()),
        ),
      ),
    );
  }

  BoxDecoration _boxDecoration() {
    return BoxDecoration(
      color: hovering[widget.index] ? kOnBackground.withOpacity(0.6) : null,
      border: Border(
        bottom: BorderSide(
          width: hovering[widget.index] ? 3 : 0,
          color: hovering[widget.index] ? kPrimary : kOnPrimary.withOpacity(0),
        ),
      ),
    );
  }


  //bodyLarge
  TextStyle _textStyle() {
    return TextStyle(
        color: hovering[widget.index]
            ? kOnPrimary
            : kOnPrimary.withOpacity(
                (widget.scrollOffset / 350).clamp(0.4, 1).toDouble()),
        fontSize: 16,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.5);
  }
}

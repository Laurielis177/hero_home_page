import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hero_home_page/components/Appbar/AppBar_Button/index.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:hero_home_page/pages/lang_page.dart';
import 'package:hero_home_page/responsive/responsive_layout.dart';

class MyAppBar extends StatefulWidget implements PreferredSizeWidget {
  const MyAppBar({Key? key, required this.scrollOffset}) : super(key: key);
  @override
  State<MyAppBar> createState() => _MyAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight * 1.3);
  final double scrollOffset;
}

class _MyAppBarState extends State<MyAppBar> {
  final title = "H.E.R.O";
  final bool hovering = false;
  final String assetName = 'assets/Icons/logo_outline.svg';

  @override
  Widget build(BuildContext context) {
    return ResponsiveLayout(
      mobileBody: AppBar(
        toolbarHeight: kToolbarHeight * 1.3,
        backgroundColor: kSurface
            .withOpacity((widget.scrollOffset / 350).clamp(0, 1).toDouble()),
        toolbarTextStyle: const AppBarTheme().toolbarTextStyle,
        shadowColor: kOnBackground
            .withOpacity((widget.scrollOffset / 350).clamp(0, 1).toDouble()),
        leading: IconButton(
          color: kOnPrimary
              .withOpacity((widget.scrollOffset / 350).clamp(0, 1).toDouble()),
          onPressed: () {
            Navigator.of(context).pushNamed('/Home');
          },
          icon: SvgPicture.asset(
            assetName,
            height: 50.0,
            width: 50.0,
          ),
        ),
        title: Text(
          title,
          style: _textStyle(),
        ),
      ),
      tabletBody: AppBar(
        toolbarHeight: widget.preferredSize.height * 1.3,
        backgroundColor: kSurface
            .withOpacity((widget.scrollOffset / 800).clamp(0, 1).toDouble()),
        toolbarTextStyle: const AppBarTheme().toolbarTextStyle,
        shadowColor: kOnBackground
            .withOpacity((widget.scrollOffset / 800).clamp(0, 1).toDouble()),
        leading: IconButton(
          color: kOnPrimary
              .withOpacity((widget.scrollOffset / 800).clamp(0, 1).toDouble()),
          onPressed: () {
            Navigator.of(context).pushNamed('/Home');
          },
          icon: SvgPicture.asset(
            assetName,
            height: 50.0,
            width: 50.0,
          ),
        ),
        title: Text(
          title,
          style: _textStyle(),
        ),
      ),
      desktopBody: AppBar(
        shape: Border(
            bottom: BorderSide(
          color: kOnPrimary.withOpacity(0.5),
          width: 0,
        )),
        toolbarHeight: kToolbarHeight * 1.3,
        backgroundColor: kSurface
            .withOpacity((widget.scrollOffset / 800).clamp(0, 1).toDouble()),
        toolbarTextStyle: const AppBarTheme().toolbarTextStyle,
        centerTitle: false,
        leading: IconButton(
          color: kOnPrimary
              .withOpacity((widget.scrollOffset / 800).clamp(0, 1).toDouble()),
          onPressed: () {
            Navigator.of(context).pushNamed('/Home');
          },
          icon: SvgPicture.asset(
            assetName,
            height: 50.0,
            width: 50.0,
          ),
        ),
        // leadingWidth: 50,
        title: Text(
          title,
          style: _textStyle(),
        ),
        actions: [
          MyAppBarButton(
            index: 0,
            scrollOffset: widget.scrollOffset,
          ),
          MyAppBarButton(
            index: 1,
            scrollOffset: widget.scrollOffset,
          ),
          MyAppBarButton(
            index: 2,
            scrollOffset: widget.scrollOffset,
          ),
          MyAppBarButton(
            index: 3,
            scrollOffset: widget.scrollOffset,
          ),
          TextButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => const LanguageView(),),
              );
              setState(() {
              });
            },
            child: const Icon(
              Icons.language,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  TextStyle _textStyle() {
    return TextStyle(
        color: hovering
            ? kOnPrimary
            : kOnPrimary.withOpacity(
                (widget.scrollOffset / 350).clamp(0.4, 1).toDouble()),
        fontSize: 24,
        fontWeight: FontWeight.bold,
        letterSpacing: 0.5);
  }
}

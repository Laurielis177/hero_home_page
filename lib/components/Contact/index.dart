import 'package:flutter/material.dart';
import 'package:hero_home_page/components/Contact/Contact_Form/index.dart';
import 'package:hero_home_page/components/Contact/Contact_Map/index.dart';
import 'package:hero_home_page/components/Title/index.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:hero_home_page/generated/locale_keys.g.dart';
import 'package:hero_home_page/responsive/responsive_layout.dart';

class MyContact extends StatefulWidget {
  const MyContact({Key? key}) : super(key: key);

  @override
  State<MyContact> createState() => _MyContactState();
}

class _MyContactState extends State<MyContact> {
  @override
  Widget build(BuildContext context) {
    final textScaleFactor = MediaQuery.of(context).textScaleFactor;
    final weight = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    return ResponsiveLayout(
      mobileBody: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: kDefaultPadding * 5,
          horizontal: kDefaultPadding * 2
        ),
        child: Column(
          children: [
            const MyTitle(title: LocaleKeys.contact_title_title, subtitle: ''),
            const Padding(
              padding: EdgeInsets.only(right: kDefaultPadding),
              child: MyContactForm(),
            ),
            Padding(
              padding: const EdgeInsets.only(top: kDefaultPadding * 3),
              child: SizedBox(
                width: textScaleFactor * weight,
                height: textScaleFactor * height,
                child: const MyContactMap(),
              ),
            ),
          ],
        ),
      ),
      tabletBody: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: kDefaultPadding * 5,
        ),
        child: FractionallySizedBox(
          widthFactor: kDefaultWidthFactor,
          child: Column(
            children: [
              const MyTitle(title: LocaleKeys.contact_title_title, subtitle: ''),
              Row(
                children: const [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(right: kDefaultPadding),
                      child: MyContactForm(),
                    ),
                  ),
                  Expanded(
                    child: SizedBox(
                      width: 200.0,
                      height: 500.0,
                      child: MyContactMap(),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      desktopBody: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: kDefaultPadding * 5,
        ),
        child: FractionallySizedBox(
          widthFactor: kDefaultWidthFactor,
          child: Column(
            children: [
              const MyTitle(title: LocaleKeys.contact_title_title, subtitle: ''),
              Row(
                children: const [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(right: kDefaultPadding),
                      child: MyContactForm(),
                    ),
                  ),
                  Expanded(
                    child: SizedBox(
                      width: 200.0,
                      height: 500.0,
                      child: MyContactMap(),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

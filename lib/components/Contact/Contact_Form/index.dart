import 'package:flutter/material.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:hero_home_page/generated/locale_keys.g.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';
import 'package:easy_localization/easy_localization.dart';



class MyContactForm extends StatefulWidget {
  const MyContactForm({super.key});

  @override
  State<MyContactForm> createState() => _MyContactFormState();
}

class _MyContactFormState extends State<MyContactForm> {
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final messageController = TextEditingController();
  final GlobalKey _formKey = GlobalKey<FormState>();

  void addContact() async {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(
          LocaleKeys.contact_popup.tr(),
          style: const TextStyle(fontSize: kFontText, color: kOnBackground),
        ),
        elevation: 5,
      ),
    );
    await saveTodo(
        nameController.text, emailController.text, messageController.text);
    setState(() {
      nameController.clear();
      emailController.clear();
      messageController.clear();
    });
  }

  Future<void> saveTodo(String name, String email, String message) async {
    final contact = ParseObject('Contact')
      ..set('name', name)
      ..set('email', email)
      ..set('message', message);
    await contact.save();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey, //设置globalKey，用于后面获取FormState
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: kDefaultPadding),
            child: TextFormField(
              autofocus: true,
              controller: nameController,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return LocaleKeys.contact_validate_text_name.tr();
                }
                return null;
              },
              decoration: InputDecoration(
                hintText: LocaleKeys.contact_hint_text_name.tr(),
                prefixIcon: const Icon(Icons.person),
                border: const OutlineInputBorder(),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: kDefaultPadding),
            child: TextFormField(
              controller: emailController,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return LocaleKeys.contact_validate_text_email.tr();
                }
                return null;
              },
              decoration: InputDecoration(
                hintText: LocaleKeys.contact_hint_text_email.tr(),
                prefixIcon: const Icon(Icons.email),
                border: const OutlineInputBorder(),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: kDefaultPadding),
            child: TextFormField(
              controller: messageController,
              decoration: InputDecoration(
                labelText: LocaleKeys.contact_hint_text_message.tr(),
                border: const OutlineInputBorder(),
              ),
              maxLength: 200,
              maxLines: 8,
            ),
          ),
          // 登录按钮
          Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: OutlinedButton(
                    style: const ButtonStyle(
                      backgroundColor:
                          MaterialStatePropertyAll<Color>(kPrimary),
                      foregroundColor:
                          MaterialStatePropertyAll<Color>(kOnPrimary),
                    ),
                    onPressed: () {
                      addContact();
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: const Text(
                        LocaleKeys.contact_button,
                        style: TextStyle(
                          color: kOnPrimary,
                          fontSize: kFontText,
                        ),
                      ).tr(),
                    ),
                  ),
                )
              ],
            ),
          )
        ]));
  }
}

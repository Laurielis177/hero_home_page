import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


class MyContactMap extends StatefulWidget {
  const MyContactMap({super.key});

  @override
  State<MyContactMap> createState() => _MyContactMapState();
}

class _MyContactMapState extends State<MyContactMap> {
  late GoogleMapController mapController;

  final LatLng center = const LatLng(25.0357706, 121.5748757);

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: center,
            zoom: 11.0,
          ),
        );
  }
}

//(25.0357706, 121.5748757)
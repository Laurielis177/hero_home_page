import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:hero_home_page/components/Title/index.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:hero_home_page/generated/locale_keys.g.dart';
import 'package:hero_home_page/responsive/responsive_layout.dart';

class MyCrew extends StatelessWidget {
  MyCrew({Key? key}) : super(key: key);

  final List<String> varName = [' Alex Yang', 'Michael Pan', 'Lauriel Huang'];
  final List<String> varAvatar = [
    "assets/images/carousel3.jpg",
    "assets/images/carousel3.jpg",
    "assets/images/carousel3.jpg"
  ];
  final List<String> varPosition = [
    ' Alex Yang',
    'Michael Pan',
    'Lauriel Huang'
  ];
  final List<String> varDesc = [' Alex Yang', 'Michael Pan', 'Lauriel Huang'];

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return ResponsiveLayout(
      mobileBody: Column(children: [
        MyTitle(title: LocaleKeys.crew_title_title.tr(), subtitle: ''),
        FractionallySizedBox(
          widthFactor: 0.9,
          child: SizedBox(
            height: height,
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 1, //每行三列
                childAspectRatio: MediaQuery.of(context).size.width /
                    (MediaQuery.of(context).size.height * 0.8),
              ),
              itemCount: varName.length,
              itemBuilder: (context, index) {
                return Card(
                  elevation: 2,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  color: Theme.of(context).colorScheme.surfaceVariant,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 3,
                        child: Image(
                          image: AssetImage(varAvatar[index]),
                          fit: BoxFit.cover,
                        ),
                      ),
                      ListTile(
                        title: AutoSizeText(varName[index]),
                        subtitle: AutoSizeText(varPosition[index]),
                      ),
                      Expanded(
                        child: ListTile(
                          subtitle: AutoSizeText(
                            varDesc[index],
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ]),
      tabletBody: Column(children: [
        MyTitle(title: LocaleKeys.crew_title_title.tr(), subtitle: ''),
        FractionallySizedBox(
          widthFactor: 0.8,
          child: SizedBox(
            height: height,
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, //每行三列
                childAspectRatio: MediaQuery.of(context).size.width /
                    (MediaQuery.of(context).size.height * 1.5),
              ),
              itemCount: varName.length,
              itemBuilder: (context, index) {
                return Card(
                  elevation: 2,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  color: Theme.of(context).colorScheme.surfaceVariant,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 3,
                        child: Image(
                          image: AssetImage(varAvatar[index]),
                          fit: BoxFit.cover,
                        ),
                      ),
                      ListTile(
                        title: AutoSizeText(varName[index]),
                        subtitle: AutoSizeText(varPosition[index]),
                      ),
                      Expanded(
                        child: ListTile(
                          subtitle: AutoSizeText(
                            varDesc[index],
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ]),
      desktopBody: Column(children: [
        MyTitle(title: LocaleKeys.crew_title_title.tr(), subtitle: ''),
        FractionallySizedBox(
          widthFactor: kDefaultWidthFactor,
          child: SizedBox(
            height: height,
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3, //每行三列
                childAspectRatio: MediaQuery.of(context).size.width /
                    (MediaQuery.of(context).size.height * 2.3),
              ),
              itemCount: varName.length,
              itemBuilder: (context, index) {
                return Card(
                  elevation: 2,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  color: Theme.of(context).colorScheme.surfaceVariant,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 3,
                        child: Image(
                          image: AssetImage(varAvatar[index]),
                          fit: BoxFit.cover,
                        ),
                      ),
                      ListTile(
                        title: AutoSizeText(varName[index]),
                        subtitle: AutoSizeText(varPosition[index]),
                      ),
                      Expanded(
                        child: ListTile(
                          subtitle: AutoSizeText(
                            varDesc[index],
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ]),
    );
  }
}

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:hero_home_page/pages/lang_page.dart';
import 'package:hero_home_page/pages/about_page.dart';
import 'package:hero_home_page/pages/contact_page.dart';
import 'package:hero_home_page/pages/home_page.dart';
import 'package:hero_home_page/pages/product_page.dart';

class MyDrawer extends StatefulWidget {
  MyDrawer({Key? key}) : super(key: key);
  final List<String> title = ['HOME', 'ABOUT', 'PRODUCT', 'CONTACT'];

  @override
  State<MyDrawer> createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  final title = 'H.E.R.O';
  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: kGrey,
      elevation: 20,
      width: MediaQuery.of(context).size.width * 0.4,
      child: ListView(
        // padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            child: Center(
              child: AutoSizeText(
                title,
                style: Theme.of(context).textTheme.headlineMedium,
              ),
            ),
          ),
          ListTile(
            title: AutoSizeText(
              widget.title[0],
              style: Theme.of(context).textTheme.titleSmall,
            ),
            leading: const Icon(Icons.home),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const HomePage(),
                ),
              );
            },
          ),
          ListTile(
            title: AutoSizeText(
              widget.title[1],
              style: Theme.of(context).textTheme.titleSmall,
            ),
            leading: const Icon(Icons.newspaper),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const AboutPage(),
                ),
              );
            },
          ),
          ListTile(
            title: AutoSizeText(
              widget.title[2],
              style: Theme.of(context).textTheme.titleSmall,
            ),
            leading: const Icon(Icons.radar),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const ProductPage(),
                ),
              );
            },
          ),
          ListTile(
            title: AutoSizeText(
              widget.title[3],
              style: Theme.of(context).textTheme.titleSmall,
            ),
            leading: const Icon(Icons.phone),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const ContactPage(),
                ),
              );
            },
          ),
          TextButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => const LanguageView(),),
              );
            },
            child: const Icon(
              Icons.language,
              color: Colors.white,
            ),
          )

        ],
      ),
    );
  }
}

import 'dart:async';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

class MyProductPage extends StatefulWidget {
  const MyProductPage({Key? key}) : super(key: key);

  @override
  State<MyProductPage> createState() => _MyProductPageState();
}

class _MyProductPageState extends State<MyProductPage> {
  List<ParseObject> productPageList = [];
  List<ParseObject> headingList = [];

  final QueryBuilder<ParseObject> queryProductPage =
      QueryBuilder<ParseObject>(ParseObject('ProductPage'))
        ..orderByAscending('index');

  StreamController<List<ParseObject>> streamController = StreamController();

  final LiveQuery liveQuery = LiveQuery(debug: true);
  late Subscription<ParseObject> subscription;

  @override
  void initState() {
    super.initState();
    _getParseData();
    startTimer();
  }

  void _getParseData() {
    final QueryBuilder<ParseObject> parseQuery =
        QueryBuilder<ParseObject>(ParseObject('ProductPage'));

    parseQuery.query().then((apiResponse) => {
          if (apiResponse.success && apiResponse.results != null)
            {
              for (var o in apiResponse.results!)
                {
                  _headingPointer((o as ParseObject).get("title")),
                  setState(() {
                    productPageList.add(o);
                  }),
                },
            }
        });
  }

  // second time query in Lang to get the 'description' value
  void _headingPointer(languageParse) {
    ParseObject lang = languageParse;

    final QueryBuilder<ParseObject> parseQuery =
        QueryBuilder<ParseObject>(ParseObject('Language'));

    parseQuery.whereContains('objectId', lang.objectId!);
    parseQuery.query().then((response) => {
          if (response.success && response.results != null)
            {
              for (var o in response.results!)
                {
                  setState(() {
                    headingList.add(o);
                  }),
                }
            }
        });
  }

  bool isLoading = true;
  void startTimer() {
    Timer.periodic(const Duration(seconds: 2), (t) {
      setState(() {
        isLoading = false;
      });
      t.cancel();
    });
  }

  @override
  Widget build(BuildContext context) {

    final String switchLanguage = context.locale.toString();
    setState(() {
      switchLanguage;
    });
    return SizedBox(
        child: isLoading
            ? Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Center(
              child: SizedBox(
                  width: 100,
                  height: 100,
                  child: CircularProgressIndicator()),
            ), //show this if state is loading
          ],
        )
            : Stack(
          children: [
            CarouselSlider.builder(
                options: CarouselOptions(
                    enlargeCenterPage: true,
                    height: MediaQuery.of(context).size.height / 2,
                    viewportFraction: 1.0,
                    autoPlay: true,
                    ),
                itemCount: productPageList.length,
                itemBuilder: (BuildContext context, int index,
                    int realIndex) {
                  final height = MediaQuery.of(context).size.height;
                  //*************************************
                  //Get Parse Object Values
                  final varProductPage = productPageList[index];
                  final varImage =
                  varProductPage.get<ParseFileBase>('image');
                  final varHeading =
                  headingList[index].get<String>(switchLanguage)!;
                  //*************************************
                  return CarouselSlider(
                    options: CarouselOptions(
                      enlargeCenterPage: true,
                      height: height,
                      viewportFraction: 1.0,
                      autoPlay: true,
                    ),
                    items: productPageList
                        .map(
                          (item) => Center(
                        child: Stack(
                          children: [
                            Image.network(
                              varImage!.url!,
                              fit: BoxFit.cover,
                              color: kSurface,
                              colorBlendMode:
                              BlendMode.multiply,
                              width: MediaQuery.of(context)
                                  .size
                                  .width,
                              height: MediaQuery.of(context)
                                  .size
                                  .height,
                            ),
                            Center(
                              child: FractionallySizedBox(
                                  widthFactor:
                                  kDefaultWidthFactor,
                                  child: AutoSizeText(
                                    varHeading,
                                    maxLines: 2,
                                    style: const TextStyle(
                                        color: kOnPrimary,
                                        fontSize: kFontSubTitle,
                                        fontWeight:
                                        FontWeight.w400),
                                  )),
                            )
                          ],
                        ),
                      ),
                    )
                        .toList(),
                  );
                })
          ],
        ));
  }
}

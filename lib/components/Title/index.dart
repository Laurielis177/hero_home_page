import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:hero_home_page/responsive/responsive_layout.dart';

class MyTitle extends StatelessWidget {
  const MyTitle({Key? key, required this.title, required this.subtitle})
      : super(key: key);
  final String title;
  final String subtitle;
  final String assetName = 'assets/Icons/title.svg';

  @override
  Widget build(BuildContext context) {
    return ResponsiveLayout(
      desktopBody: FractionallySizedBox(
        widthFactor: kDefaultWidthFactor,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title,
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.displaySmall,
                ).tr(),
                // SvgPicture.asset(
                //   assetName,
                //   height: 20.0,
                //   width: 20.0,
                // ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: kDefaultPadding, bottom: kDefaultPadding * 3),
              child: Text(
                subtitle,
                maxLines: 1,
                textAlign: TextAlign.center,

                style: Theme.of(context).textTheme.titleLarge,
              ).tr(),
            ),
          ],
        ),
      ),
      mobileBody: FractionallySizedBox(
        widthFactor: 0.9,
        child: Column(
          children: [
            Text(
              title,
              maxLines: 2,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.displaySmall,
            ).tr(),
            Padding(
              padding: const EdgeInsets.only(
                  top: kDefaultPadding, bottom: kDefaultPadding * 3),
              child: Text(
                subtitle,
                maxLines: 1,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.titleLarge,
              ).tr(),
            ),
          ],
        ),
      ),
      tabletBody: FractionallySizedBox(
        widthFactor: 0.8,
        child: Column(
          children: [
            Text(
              title,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.displaySmall,
            ).tr(),
            Padding(
              padding: const EdgeInsets.only(
                  top: kDefaultPadding, bottom: kDefaultPadding * 3),
              child: Text(
                subtitle,
                maxLines: 1,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.titleLarge,
              ).tr(),
            ),
          ],
        ),
      ),
    );
  }
}

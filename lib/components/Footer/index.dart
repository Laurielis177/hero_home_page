import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:hero_home_page/components/Footer/Footer_Link/index.dart';
import 'package:hero_home_page/components/Footer/Social_Icon/index.dart';
import 'package:hero_home_page/responsive/responsive_layout.dart';
import 'Footer_Info/index.dart';
import 'package:easy_localization/easy_localization.dart';

class MyFooter extends StatelessWidget {
  const MyFooter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final year = DateFormat.y().format(DateTime.now());
    return ResponsiveLayout(
      mobileBody: Container(
        decoration: const BoxDecoration(
          color: kSurface,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
              vertical: kDefaultPadding * 2, horizontal: kDefaultPadding * 2),
          child: Column(
            children: [
              const MyFooterInfo(),
              const MyFooterSocialIcon(),
              AutoSizeText(
                'Copyright © $year H.E.R.O. All rights reserved.',
                style: Theme.of(context).textTheme.labelSmall,
              ),
            ],
          ),
        ),
      ),
      tabletBody: Container(
        decoration: const BoxDecoration(
          color: kSurface,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
              vertical: kDefaultPadding * 2, horizontal: kDefaultPadding * 2),
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.only(left: kDefaultPadding),
                child: MyFooterInfo(),
              ),
              const Padding(
                padding: EdgeInsets.only(
                    right: kDefaultPadding, top: kDefaultPadding),
                child: MyFooterSocialIcon(),
              ),
              AutoSizeText(
                'Copyright © $year H.E.R.O. All rights reserved.',
                style: Theme.of(context).textTheme.labelSmall,
              ),
            ],
          ),
        ),
      ),
      desktopBody: Container(
        decoration: const BoxDecoration(
          color: kSurface,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
              vertical: kDefaultPadding * 2, horizontal: kDefaultPadding * 2),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const [
                  Padding(
                    padding: EdgeInsets.only(left: kDefaultPadding),
                    child: MyFooterInfo(),
                  ),
                  Expanded(
                    child: MyFooterLink(),
                  ),
                ],
              ),
              const Padding(
                // ignore: prefer_const_constructors
                padding: EdgeInsets.only(
                    right: kDefaultPadding, top: kDefaultPadding),
                child: MyFooterSocialIcon(),
              ),
              AutoSizeText(
                'Copyright © $year H.E.R.O. All rights reserved.',
                style: Theme.of(context).textTheme.labelSmall,
                maxLines: 1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

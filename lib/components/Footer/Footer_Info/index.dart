import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hero_home_page/components/Footer/Footer_Info/Footer_Info_Element/index.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:hero_home_page/generated/locale_keys.g.dart';

class MyFooterInfo extends StatelessWidget {
  const MyFooterInfo({Key? key}) : super(key: key);
  final String assetName = 'assets/Icons/logo_outline.svg';

  @override
  Widget build(BuildContext context) {

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: kDefaultPadding),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    right: kDefaultPadding / 2,
                  ),
                  child:SvgPicture.asset(assetName,height: 50.0, width: 50.0,),
                ),
                AutoSizeText(
                  'H.E.R.O',
                  textAlign: TextAlign.left,
                  style: _textStyle(),
                ),
              ],
            ),
          ),
          MyFooterInfoElement(text: LocaleKeys.footer_info_address.tr(), index: 0,),
          MyFooterInfoElement(text: '886-2-2759-0777', index: 1,),
          MyFooterInfoElement(text: '886-2-2759-7373', index: 2,),
          MyFooterInfoElement(text: 'michael.pan@evertiphi.com', index: 3,),
        ]);
  }

  TextStyle _textStyle() {
    return const TextStyle(
      fontSize: 24,
      fontWeight: FontWeight.w400,
      color: kOnPrimary,
    );
  }
}

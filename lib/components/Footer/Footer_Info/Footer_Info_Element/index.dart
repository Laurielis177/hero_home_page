import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hero_home_page/colorScheme/constants.dart';

class MyFooterInfoElement extends StatelessWidget {
  MyFooterInfoElement({
    super.key,
    required this.index,
    required this.text,
  });
  final String text;
  final int index;

  final List<IconData> myIcon = [
    Icons.location_on,
    Icons.phone_android,
    Icons.fax,
    Icons.email
  ];

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Padding(
        padding: const EdgeInsets.symmetric(
            horizontal: kDefaultPadding / 2, vertical: kDefaultPadding / 4),
        child: Icon(
          myIcon[index],
          color: kBackground,
        ),
      ),
      Text(
        text,
        textAlign: TextAlign.left,
        maxLines: 2,
        style: GoogleFonts.montserrat(
            fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 0.4, color: Colors.white),
      ),
    ]);
  }
}

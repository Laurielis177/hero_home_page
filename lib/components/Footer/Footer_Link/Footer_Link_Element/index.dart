import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class MyFooterLinkElement extends StatelessWidget {
  MyFooterLinkElement({
    super.key,
    required this.link,
    required this.varUrl,
  });
  final String varUrl;
  final String link;
  late final Uri _url = Uri.parse(varUrl);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: _launchUrl,
      child: AutoSizeText(
        link,
        style: Theme.of(context).textTheme.bodyLarge,
      ),
    );
  }

  Future<void> _launchUrl() async {
    if (!await launchUrl(_url)) {
      throw 'Could not launch $_url';
    }
  }
}

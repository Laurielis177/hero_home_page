import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hero_home_page/components/Footer/Footer_Link/Footer_Link_Element/index.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

class MyFooterLink extends StatefulWidget {
  const MyFooterLink({super.key});

  @override
  State<MyFooterLink> createState() => _MyFooterLinkState();
}

class _MyFooterLinkState extends State<MyFooterLink> {
  List<ParseObject> footerLinkList = [];
  List<ParseObject> linkNameList = [];

  StreamController<List<ParseObject>> streamController = StreamController();

  // first time query in Award
  void _getParseData() {
    final QueryBuilder<ParseObject> parseQuery =
        QueryBuilder<ParseObject>(ParseObject('FooterLink'));

    parseQuery.query().then((apiResponse) => {
          if (apiResponse.success && apiResponse.results != null)
            {
              for (var o in apiResponse.results!)
                {
                  _linkNamePointer((o as ParseObject).get("linkName")),
                  setState(() {
                    footerLinkList.add(o);
                  }),
                },
            }
        });
  }

  // second time query in Lang to get the 'description' value
  void _linkNamePointer(languageParse) {
    ParseObject lang = languageParse;

    final QueryBuilder<ParseObject> parseQuery =
        QueryBuilder<ParseObject>(ParseObject('Language'));

    parseQuery.whereContains('objectId', lang.objectId!);
    parseQuery.query().then((response) => {
          if (response.success && response.results != null)
            {
              for (var o in response.results!)
                {
                  setState(() {
                    linkNameList.add(o);
                  }),
                }
            }
        });
  }

  bool isLoading = true;
  void startTimer() {
    Timer.periodic(const Duration(seconds: 2), (t) {
      setState(() {
        isLoading = false;
      });
      t.cancel();
    });
  }

  @override
  void initState() {
    super.initState();
    _getParseData();
    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final String switchLanguage = context.locale.toString();
    setState(() {
      switchLanguage;
    });

    return SizedBox(
        height: height * 0.4,
        width: width,
        child: isLoading
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: const <Widget>[
                  Center(
                    child: SizedBox(
                        width: 100,
                        height: 100,
                        child: CircularProgressIndicator()),
                  ), //show this if state is loading
                ],
              )
            : GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  mainAxisSpacing: 20,
                  crossAxisCount: 3, //每行三列
                  childAspectRatio: 6.2, //显示区域宽高相等
                ),
                itemCount: footerLinkList.length,
                itemBuilder: (context, index) {
                  //*************************************
                  //Get Parse Object Values
                  final varFooter = footerLinkList[index];
                  final varUrl = varFooter.get<String>('url')!;
                  final varName =
                      linkNameList[index].get<String>(switchLanguage);
                  //*************************************
                  return MyFooterLinkElement(
                    link: varName!,
                    varUrl: varUrl,
                  );
                }));
  }
}

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:url_launcher/url_launcher.dart';

class MyFooterSocialIcon extends StatelessWidget {
  const MyFooterSocialIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<String> varUrl = [
      'https://api.flutter.dev/flutter/material/Icons/youtube_searched_for-constant.html',
      'https://api.flutter.dev/flutter/material/Icons/youtube_searched_for-constant.html',
      'https://api.flutter.dev/flutter/material/Icons/youtube_searched_for-constant.html'
    ];

    // ignore: no_leading_underscores_for_local_identifiers
    Future<void> _launchUrl(List<String> varUrl, int index) async {
      // ignore: no_leading_underscores_for_local_identifiers
      Uri _url = Uri.parse(varUrl[index]);
      if (!await launchUrl(_url)) {
        throw 'Could not launch $_url';
      }
    }

    return SizedBox(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: kDefaultPadding),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  right: kDefaultPadding, bottom: kDefaultPadding * 1.2),
              child: IconButton(
                icon: const CircleAvatar(
                    radius: 20,
                    backgroundColor: kOnPrimary,
                    child: FaIcon(FontAwesomeIcons.google, color: kPrimary,)
                ),
                onPressed: () {
                  _launchUrl(varUrl, 0);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  right: kDefaultPadding, bottom: kDefaultPadding * 1.2),
              child: IconButton(
                icon: const CircleAvatar(
                    radius: 20,
                    backgroundColor: kOnPrimary,
                    child: FaIcon(FontAwesomeIcons.facebook, color: kPrimary,)
                ),
                onPressed: () {
                  _launchUrl(varUrl, 1);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  right: kDefaultPadding, bottom: kDefaultPadding * 1.2),
              child: IconButton(
                icon: const CircleAvatar(
                    radius: 20,
                    backgroundColor: kOnPrimary,
                    child: FaIcon(FontAwesomeIcons.youtube, color: kPrimary,)
                ),
                onPressed: () {
                  _launchUrl(varUrl, 2);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

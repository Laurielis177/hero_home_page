import 'package:flutter/material.dart';

//Colors
const double kDefaultPadding = 20.0;
const double kDefaultWidthFactor = 0.7;
const double kFontTitle = 32;
const double kFontSubTitle = 24;
const double kFontText = 16;




const kPrimary = Color(0xFFBB4656);
const kOnPrimary = Color(0xFFFFFFFF);
const kPrimaryContainer = Color(0xFFFFDADB);
const kOnPrimaryContainer = Color(0xFF40000E);
const kSecondary = Color(0xFFF6CA45);
const kSecondaryContainer = Color(0xFFFFE08D);
const kOnSecondaryContainer = Color(0xFF241A00);
const kBackground = Color(0xFFEEEEEE);
const kOnBackground = Color(0xFF5A5A5A);
const kSurface = Color(0xFF797979);
const kGrey = Color(0xFFD9D9D9);
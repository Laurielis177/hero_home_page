import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:hero_home_page/pages/about_page.dart';
import 'package:hero_home_page/pages/contact_page.dart';
import 'package:hero_home_page/pages/home_page.dart';
import 'package:hero_home_page/pages/product_page.dart';
import 'package:hero_home_page/pages/splash_page.dart';
import 'package:hero_home_page/colorScheme/theme_data.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  const keyApplicationId = 'aB8hNUskqdZgQG80JrKTIiVP2OB9QglVPvw6wBKx';
  const keyClientKey = 'cQmfOIQ9hpwdMXInQ9ye2yLfhTAtinLxJt1nWYS3';
  const keyParseServerUrl = 'https://parseapi.back4app.com';
  await Parse().initialize(keyApplicationId, keyParseServerUrl,
      clientKey: keyClientKey,
      liveQueryUrl: 'https://herohomepage.b4a.io',
      debug: true,
      autoSendSessionId: true);
  runApp(EasyLocalization(
    supportedLocales: const [
      Locale('en', 'US'),
      Locale('zh', 'TW'),
      Locale('ar', 'DZ'),
      Locale('de', 'DE'),
      Locale('ru', 'RU'),
    ],
    path: 'resources/langs',
    child: const MyApp(),
  ));

}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'H.E.R.O 地震檢測',
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      theme: _themeData(),
      //================================================================
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      //================================================================
      routes: {
        '/': (_) => const SplashScreen(),
        '/Home': (_) => const HomePage(),
        '/About': (_) => const AboutPage(),
        '/Product': (_) => const ProductPage(),
        '/Contact': (_) => const ContactPage(),
      },
      // home: const SplashScreen(),
    );
  }

  ThemeData _themeData() {
    return ThemeData(
      // colorSchemeSeed: createMaterialColor(kOnBackground),
      useMaterial3: true,
      textTheme: _textTheme(),
      appBarTheme: _appBarTheme(),
      iconTheme: const IconThemeData(color: kOnPrimary),
      primarySwatch: createMaterialColor(kOnBackground),
      scaffoldBackgroundColor: kOnBackground,
    );
  }

  TextTheme _textTheme() {
    return TextTheme(
      displayLarge: GoogleFonts.openSans(
          fontSize: 95, fontWeight: FontWeight.w600, letterSpacing: -1.5),
      displayMedium: GoogleFonts.openSans(
          fontSize: 59, fontWeight: FontWeight.w600, letterSpacing: -0.5),
      displaySmall: GoogleFonts.openSans(
          fontSize: 36, fontWeight: FontWeight.w700, color: kPrimary),
      headlineMedium: GoogleFonts.openSans(
          fontSize: 34,
          fontWeight: FontWeight.w600,
          letterSpacing: 0.25,
          color: kOnPrimary),
      headlineSmall: GoogleFonts.openSans(
          fontSize: 24, fontWeight: FontWeight.w400, color: kOnBackground),
      titleLarge: GoogleFonts.openSans(
          fontSize: 20,
          fontWeight: FontWeight.w500,
          letterSpacing: 0.15,
          color: kOnBackground,
          height: 1.8),
      titleMedium: GoogleFonts.openSans(
          fontSize: 16,
          fontWeight: FontWeight.w400,
          letterSpacing: 0.15,
          color: kOnBackground,
          height: 1.8),
      titleSmall: GoogleFonts.openSans(
          fontSize: 14,
          fontWeight: FontWeight.w500,
          letterSpacing: 0.1,
          color: kOnBackground),
      bodyLarge: GoogleFonts.montserrat(
          fontSize: 16,
          fontWeight: FontWeight.w400,
          letterSpacing: 0.5,
          color: kOnPrimary),
      bodyMedium: GoogleFonts.montserrat(
          fontSize: 14, fontWeight: FontWeight.w400, letterSpacing: 0.25),
      labelLarge: GoogleFonts.montserrat(
          fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 1.25),
      bodySmall: GoogleFonts.montserrat(
          fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 0.4),
      labelSmall: GoogleFonts.montserrat(
          fontSize: 10,
          fontWeight: FontWeight.w400,
          letterSpacing: 1.5,
          color: kGrey),
    );
  }

  AppBarTheme _appBarTheme() {
    return const AppBarTheme(
      //headlineMedium
      toolbarTextStyle: TextStyle(
          fontSize: 34,
          fontWeight: FontWeight.w400,
          letterSpacing: 0.25,
          color: kPrimary),
      //Title Bar
      titleTextStyle: TextStyle(
        fontSize: 34,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.25,
        color: kOnPrimary,
      ),
      iconTheme: IconThemeData(
        size: 30,
        color: kOnPrimary,
      ),
    );
  }
}

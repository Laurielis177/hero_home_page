import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:hero_home_page/colorScheme/constants.dart';

class LanguageView extends StatelessWidget {
  const LanguageView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kOnPrimary,
        iconTheme: const IconThemeData(color: kPrimary),
        elevation: 5,
      ),
      body: Container(
        color: kOnPrimary,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.only(top: 26),
              margin: const EdgeInsets.symmetric(
                horizontal: 24,
              ),
              child: const Text(
                'Choose language',
                style: TextStyle(
                  color: kOnBackground,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
              ),
            ),
            SwitchListTileMenuItem(
                title: '繁體中文',
                subtitle: '中文',
                locale: context.supportedLocales[1]),
            const _Divider(),
            // SwitchListTileMenuItem(
            //     title: 'عربي',
            //     subtitle: 'عربي',
            //     locale:
            //         context.supportedLocales[2] //BuildContext extension method
            //     ),
            // const _Divider(),
            SwitchListTileMenuItem(
                title: 'English',
                subtitle: 'English',
                locale: context.supportedLocales[0]),
            const _Divider(),
            // SwitchListTileMenuItem(
            //     title: 'German',
            //     subtitle: 'German',
            //     locale: context.supportedLocales[3]),
            // const _Divider(),
            // SwitchListTileMenuItem(
            //     title: 'Русский',
            //     subtitle: 'Русский',
            //     locale: context.supportedLocales[4]),
            // const _Divider(),

          ],
        ),
      ),
    );
  }
}

class _Divider extends StatelessWidget {
  const _Divider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 24,
      ),
      child: const Divider(
        color: Colors.grey,
      ),
    );
  }
}

class SwitchListTileMenuItem extends StatefulWidget {
  const SwitchListTileMenuItem({
    Key? key,
    required this.title,
    required this.subtitle,
    required this.locale,
  }) : super(key: key);

  final String title;
  final String subtitle;
  final Locale locale;

  bool isSelected(BuildContext context) => locale == context.locale;

  @override
  State<SwitchListTileMenuItem> createState() =>
      SwitchListTileMenuItemState();
}

class SwitchListTileMenuItemState extends State<SwitchListTileMenuItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 10, right: 10, top: 5),
      decoration: BoxDecoration(
        border: widget.isSelected(context)
            ? Border.all(color: kPrimary)
            : null,
      ),
      child: ListTile(
          dense: true,
          // isThreeLine: true,
          title: Text(
            widget.title,
          ),
          subtitle: Text(
            widget.subtitle,
          ),
          onTap: () async {
            log(widget.locale.toString(), name: toString());
            await context.setLocale(widget.locale);
            if (!mounted) return;
            Navigator.pop(context);
            setState(() {
              // if (kDebugMode) {
              //   print("=============${widget.locale}");
              // }
            });
          }),
    );
  }
}

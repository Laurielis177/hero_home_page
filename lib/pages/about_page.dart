import 'package:flutter/material.dart';
import 'package:hero_home_page/components/Appbar/index.dart';
import 'package:hero_home_page/components/CompanyIntro/index.dart';
import 'package:hero_home_page/components/Crew/index.dart';
import 'package:hero_home_page/components/Drawer/index.dart';
import 'package:hero_home_page/components/Footer/index.dart';
import 'package:hero_home_page/components/Header/index.dart';
import 'package:hero_home_page/components/Award/index.dart';
import 'package:hero_home_page/colorScheme/constants.dart';
import 'package:hero_home_page/generated/locale_keys.g.dart';

class AboutPage extends StatefulWidget {
  const AboutPage({Key? key}) : super(key: key);

  @override
  State<AboutPage> createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  late ScrollController _scrollController;
  double _scrollControllerOffset = 0.0;
  bool _showBackToTopButton = false;

  _scrollListener() {
    setState(() {
      _scrollControllerOffset = _scrollController.offset;
    });
  }

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    _scrollController.addListener(() {
      setState(() {
        if (_scrollController.offset >= 400) {
          _showBackToTopButton = true; // show the back-to-top button
        } else {
          _showBackToTopButton = false; // hide the back-to-top button
        }
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose(); // dispose the controller
    super.dispose();
  }

  void _scrollToTop() {
    _scrollController.animateTo(0,
        duration: const Duration(seconds: 1),
        curve: Curves.easeInOutCubicEmphasized);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: MyAppBar(
        scrollOffset: _scrollControllerOffset,
      ),
      endDrawer: MyDrawer(),
      backgroundColor: kBackground,
      floatingActionButton: _showBackToTopButton == false ? null : _fab(),
      extendBodyBehindAppBar: true,
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          children:[
            const MyHeader(title: LocaleKeys.company_title_title),
            const MyCompanyIntro(),
            MyCrew(),
            const MyAward(),
            const MyFooter(),
          ],
        ),
      ),
    );
  }

  FloatingActionButton _fab() {
    return FloatingActionButton.small(
      onPressed: _scrollToTop,
      backgroundColor: kOnPrimary,
      elevation: 20,
      child: const Icon(
        Icons.keyboard_arrow_up,
        color: kPrimary,
        size: 35,
      ),
    );
  }
}
